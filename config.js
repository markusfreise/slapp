/*
    SLAPP
    =====    
    A software for Poetry Slams
    
    Version 1
    
    by Markus Freise

	This Source Code Form is subject to the terms 
	of the Mozilla Public License, v. 2.0. If a copy 
	of the MPL was not distributed with this file, 
	You can obtain one at http://mozilla.org/MPL/2.0/.
    
    E-Mail: markus@freise.de

*/

// Name of Slam

_slam_name = "Poetry Slam";
_slam_sub  = "The point is poetry.";

// Number of votes per poet

_slam_votes = 7;

// Number of "winners"

_slam_finalisten = 3;

// Show bumper-video betwenn screens?

_show_bumper = 1;

/************************************************************************************************/
/*
/* Please ignore the following.
*/

// Path to Wordpress-Root

_root = "";

// Passcode to retrieve events

_pass = "";

// Post-ID of final round

_final = 3;



